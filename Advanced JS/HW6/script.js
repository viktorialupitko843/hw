/**
 Теоретичне питання
 Поясніть своїми словами, як ви розумієте поняття асинхронності у
 Javascript

 У JavaScript є однопотокова модель виконання, що означає, що він не може виконувати
 задачі зі затримкою прямо в основному потоці. Однак асинхронні задачі, такі як таймери
 (setTimeout, setInterval), робота з промісами та ключові слова async/await, можуть бути виконані в браузері.

 В коді JavaScript виконання відбувається послідовно, від верху до низу, синхронно.
 Але асинхронні операції дозволяють виконати певні завдання не відразу, а після певного часу
 чи після завершення певної події. Цей час може бути заданий користувачем (через setTimeout),
 або визначатися динамічно (наприклад, при отриманні даних з сервера).

 */

const button = document.querySelector('button');
const container = document.querySelector('.container');

class Card {
    constructor(data) {
        this.data = data;
    }

    render() {
        container.innerHTML = ''

        const {timezone, country, region, city, regionName } = this.data;

        container.insertAdjacentHTML('beforeend', `
        <span>Ви знаходитесь:</span>
        <span>Континент - ${timezone.split('/')[0]}</span>
        <span>Країна - ${country}</span>
        <span>Регіон - ${region}</span>
        <span>Місто - ${city}</span>
        <span>Район - ${regionName}</span>
        `)
    }
}

const getYourIp = async () => {

    try {
        const { data: { ip } } = await axios.get('https://api.ipify.org/?format=json');

        const { data } = await axios.get(`http://ip-api.com/json/${ip}`);

        return data;
    } catch (err) {
        console.log(err)
    }
}

button.addEventListener('click', async () => {
    const data =  await getYourIp();

    new Card(data).render()
})
