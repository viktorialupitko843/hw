"use strict";
/*
AJAX - це технологія, яка дозволяє веб-сторінці взаємодіяти з сервером без
необхідності перезавантаження всієї сторінки.

 */

const container = document.querySelector('.container');
const preloader = document.querySelector('.load');

const url = 'https://ajax.test-danit.com/api/swapi/films';

fetch(url).then(res => res.json())
    .then(data => {
        preloader.classList.add('load_hide');
        data.forEach(({name, characters, episodeId, openingCrawl}) => {
            container.insertAdjacentHTML('beforeend', `
            <h1>${name}</h1>
            <h4>Number of episode: ${episodeId}</h4>
            <p>${openingCrawl}</p>
        `);

            const charactersList = document.createElement('ul');
            container.append(charactersList);

            characters.forEach(character => {
                fetch(character).then(res => res.json())
                    .then(({name}) => {
                        charactersList.insertAdjacentHTML('beforeend', `
                        <li>${name}</li>
                    `)
                    })
            })
        })
    })