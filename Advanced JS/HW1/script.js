/*
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата).
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
 */

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(lang, ...args) {
        super(...args);
        this.lang = lang;
    }
    get salary() {
        return super.salary * 3;
    }
}


const employee1 = new Programmer('C#', 'Bob', 20, 30000);
const employee2 = new Programmer('JS', 'John', 30, 15000);
const employee3 = new Programmer('Java', 'Elena', 25, 40000);

console.log(employee1, employee2, employee3);
