"use strict";

/*
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.


В JavaScript конструкція try...catch використовується для обробки винятків, таких як помилки вводу-виводу,
 відсутність з'єднання з базою даних або при роботі з сервером. Вона дозволяє програмістам перехоплювати
 ці помилки і обробляти їх, забезпечуючи стабільність програми та уникнення аварійного завершення роботи програми.
 */

/*
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось
в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність
(в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає,
 в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
 */

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const container = document.querySelector('#root');
const list = document.createElement('ul');
container.append(list);

class IsValueError extends Error {
    constructor(name) {
        super(`The ${name} property is not in the array`);
        this.name = 'IsValueError'
    }
}

class Book{
    constructor(name, price, author) {
        this.name = name;
        this.price = price;
        this.author = author;
    }
    bookValidation(){
        if (!this.name){
            throw new IsValueError('name');
        }
        if (!this.author){
            throw new IsValueError('author');
        }
        if (!this.price){
            throw new IsValueError('price');
        }
    }
    render(){
        this.bookValidation();
        list.insertAdjacentHTML('beforeend', `
            <li>${this.author}</li>
            <li>${this.name}</li>
            <li>${this.price}</li>
        `);
    }
}

books.forEach(elem => {
    try{
        new Book(elem.name, elem.price, elem.author).render();
    } catch (err){
        console.warn(err);
    }
})
