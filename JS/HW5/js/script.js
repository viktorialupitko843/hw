"use strict";

/*
1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, 
що отримано числа). 
Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
 */

function quotientNumbers() {
    let firstNumber = +prompt('Enter first number');
    while (isNaN(firstNumber)) {
        alert('Enter correct data');
        firstNumber = +prompt('Enter first number again, please');
    }

    let secondNumber = +prompt('Enter second number');
    while (isNaN(secondNumber)) {
        alert('Enter correct data');
        secondNumber = +prompt('Enter second number again, please');
    }
    return alert(`The result of division ${firstNumber / secondNumber}`);
}
quotientNumbers();

/*
2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
Сюди може бути введено +, -, *, /. Провалідувати отримане значення.
Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.
 */

let firstNumber;
do {
    firstNumber = +prompt('Enter first number');
} while (isNaN(firstNumber))

let secondNumber
do {
    secondNumber = +prompt('Enter second number');
} while (isNaN(secondNumber))

const operator = prompt('Enter mathematical operation +, -, /, *');
if (operator !== '+' && operator !== '-' && operator !== '*' && operator !== '/'){
    alert('Such an operation does not exist');
}

function calcNumbers(numberOne, numberTwo, operator) {
    switch (operator){
        case "+":{
            return numberOne + numberTwo;
        }
        case "-":{
            return numberOne - numberTwo;
        }
        case "*":{
            return numberOne * numberTwo;
        }
        case "/":{
            return numberOne / numberTwo;
        }
    }
}

console.log(calcNumbers(firstNumber, secondNumber, operator));