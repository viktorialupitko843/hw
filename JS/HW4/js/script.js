"use strict";

/*
1. Запитайте у користувача два числа.
Перевірте, чи є кожне з введених значень числом.
Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.
 */

let firstNumber = prompt('Enter first number');

while (!firstNumber || isNaN(firstNumber)) {
   firstNumber = prompt('Enter first number again');
}

let secondNumber = prompt('Enter second number');

while (!secondNumber || isNaN(secondNumber)) {
    secondNumber = prompt('Enter second number again')
}

if (firstNumber <= secondNumber) {
    for (let i = +firstNumber; i <= +secondNumber; i++) {
        console.log(i);
    }
} else {
    for (let i = +secondNumber; i <= +firstNumber; i++) {
        console.log(i);
    }
}

/*
2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом.
Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.
 */

let number;

do {
   number = +prompt('Enter your number');

    if (number % 2 === 0) {
        alert('This is an even number');
    } else {
        alert('This is not even number');
    }
} while (number % 2 !== 0)