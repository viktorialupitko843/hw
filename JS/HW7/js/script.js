"use strict";

/*
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
 */
function isPalindrome(string) {
    const cleanString = string.replace(/[^a-zA-Z]/g, '').toLowerCase();

    const reversedStr = cleanString.split('').reverse().join('');

    return cleanString === reversedStr;
}

console.log(isPalindrome("no lemon, no melon"));

/*
2. Створіть функцію, яка перевіряє довжину рядка.
 Вона приймає рядок, який потрібно перевірити, максимальну довжину
 і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший.
Ця функція стане в нагоді для валідації форми.
 */

function checkStringLength(string, maxLength) {
    return string.length <= maxLength;
}

const result1 = checkStringLength('string', 10);
console.log(result1);

const result2 = checkStringLength('string', 3);
console.log(result2);

/*
3. Створіть функцію, яка визначає скільки повних років користувачу.
Отримайте дату народження користувача через prompt.
Функція повина повертати значення повних років на дату виклику функцію.
 */

function checkFullYears() {
    const birthdayDate = prompt("Enter your birthday date: YYYY-MM-DD:");
    const data = new Date(birthdayDate);

    const currentDate = new Date();

    const userAge = currentDate.getFullYear() - data.getFullYear();

    if (currentDate.getMonth() < data.getMonth() || (currentDate.getMonth() === data.getMonth()
            && currentDate.getDate() < data.getDate()))
    {
        return { age: userAge - 1, currentDate };
    } else {
        return { userAge, currentDate };
    }
}

const result = checkFullYears();
console.log(`Your full age: ${result.userAge}.`);
console.log(`The date the function was called: ${result.currentDate}`);