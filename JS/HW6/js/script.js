"use strict";

/*
1. Створіть об'єкт product з властивостями name, price та discount.
Додайте метод для виведення повної ціни товару з урахуванням знижки.
Викличте цей метод та результат виведіть в консоль.
 */

const product = {
    name: 'laptop',
    price: 20000,
    discount: 20,
    fullPrice() {
        const sizeDiscount = this.price * this.discount / 100;
        return this.price - sizeDiscount;
    }
}

console.log(product.fullPrice());

/*
2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
 */

const userName = prompt('Enter your name, please');
const userAge = +prompt('Enter your age, please');


const userGreeting = (name, age) => {
    const user = {
        name,
        age,
    }

    return `Привіт, мене звати ${user.name}, мені ${user.age} років`;
}

alert(userGreeting(userName, userAge));