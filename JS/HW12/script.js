"use strict";

/*
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
2. Яка різниця між event.code() та event.key()?
3. Які три події клавіатури існує та яка між ними відмінність?

Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, 
повинна фарбуватися в синій колір. При цьому якщо якась 
інша літера вже раніше була пофарбована в синій колір - 
вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у 
синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, 
а кнопка Enter знову стає чорною.
*/

document.addEventListener('keydown', (event) => {
    const keyPressed = event.key.toUpperCase();
    const buttons = document.querySelectorAll('.btn');

    buttons.forEach((button) => {
        const buttonText = button.textContent.trim().toUpperCase();

        if (buttonText === keyPressed) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    });
});


 


