## 1. Опишіть своїми словами що таке Document Object Model (DOM)

DOM подібний до HTML-коду та відображає об'єктно-орієнтовану структуру веб-сторінки. Він дозволяє змінювати вміст
документа веб-сторінки за допомогою JavaScript.

## 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

- innerText не може містити HTML теги;
- innerHTML містить HTML теги;

## 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

- getElementById: Використання document.getElementById(id) для отримання елемента за його ідентифікатором (ID).
- getElementsByClassName: Використання document.getElementsByClassName(className) для отримання колекції елементів за
  їхньою класом.
- getElementsByTagName: Використання document.getElementsByTagName(tagName) для отримання колекції елементів за їхнім
  тегом.
- querySelector: Використання document.querySelector(selector) для вибору першого елемента, який відповідає
  CSS-селектору.
- querySelectorAll: Використання document.querySelectorAll(selector) для отримання всіх елементів, які відповідають
  CSS-селектору.

getElementById може бути ефективним, якщо є унікальний ідентифікатор для елемента. querySelector та
querySelectorAll дозволяють використовувати потужні CSS-селектори для вибору елементів.

## 4. Яка різниця між nodeList та HTMLCollection?

HTMLCollection завжди підтримує живе оновлення свого вмісту, тобто автоматично відображає зміни у реальному часі. У той
час як NodeList є статичною колекцією, яка не автоматично оновлюється при зміні структури документа. Крім того, NodeList
дозволяє використовувати метод forEach() для ітерації по елементах колекції.
