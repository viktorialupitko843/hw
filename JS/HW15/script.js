document.getElementById('changeButton').addEventListener('click', function() {
    setTimeout(function() {
        const contentDiv = document.getElementById('contentDiv');

        contentDiv.textContent = 'Операція виконана успішно!';
    }, 3000);
});

const countdownElement = document.getElementById('countdown');

let countdownValue = 10;

countdownElement.textContent = countdownValue;

function countdown() {
    countdownValue--;

    countdownElement.textContent = countdownValue;

    if (countdownValue === 1) {
        countdownElement.textContent = 'Зворотній відлік завершено';

        clearInterval(timerInterval);
    }
}

const timerInterval = setInterval(countdown, 1000);