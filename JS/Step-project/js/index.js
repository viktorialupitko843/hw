"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

const trainerCardTemplate = document.querySelector('#trainer-card').content;
const trainerCardsContainer = document.querySelector('.trainers-cards__container');
const modalTemplate = document.querySelector('#modal-template').content;
const sortingSection = document.querySelector('.sorting');
const sortingButtons = document.querySelectorAll('.sorting__btn');
const sidebarForm = document.querySelector('.sidebar__filters');

//add id for object
DATA.forEach((obj, index) => {
	obj.id = index + 1;
});

//disable and enable scroll function
let scrollPosition;

const disableScroll = () => {
	const body = document.body;
	scrollPosition = window.scrollY;

	body.style.overflow = 'hidden';
	body.style.position = 'fixed';
	body.style.width = '100%';
	body.style.top = `-${scrollPosition}px`
}

const enableScroll = () => {
	const body = document.body;

	body.style.overflow = '';
	body.style.position = '';
	body.style.width = '';
	body.style.top = ``;

	window.scrollTo(0, scrollPosition);
}

//render cards
const renderCards = (arr) => {
	arr.forEach(trainer => {
		const trainerCard = trainerCardTemplate.cloneNode(true);

		const  trainerImg = trainerCard.querySelector('.trainer__img');
		trainerImg.setAttribute('src', trainer.photo);

		trainerCard.querySelector('.trainer__name').innerText = `${trainer["last name"]} ${trainer["first name"]}`;
		trainerCard.querySelector('.trainer').setAttribute('data-id', trainer.id);


		trainerCardsContainer.append(trainerCard);
	});
}

renderCards(DATA);
document.querySelector('.sorting').removeAttribute('hidden');
document.querySelector('.sidebar').removeAttribute('hidden');

// Listener for modal window
trainerCardsContainer.addEventListener('click', (event) => {
	if (event.target.closest('.trainer__show-more')){
		const modalWindow = modalTemplate.cloneNode(true);
		const trainerCard = event.target.closest('.trainer');
		const cardId = +trainerCard.dataset.id;

		const trainer = DATA.find(trainer => trainer.id === cardId);

		modalWindow.querySelector('.modal__img').src = trainer.photo;
		modalWindow.querySelector('.modal__name').innerText = `${trainer["last name"]} ${trainer["first name"]}`;
		modalWindow.querySelector('.modal__point--category').innerText = `Категорія: ${trainer.category}`;
		modalWindow.querySelector('.modal__point--experience').innerText = `Досвід: ${trainer.experience}`;
		modalWindow.querySelector('.modal__point--specialization').innerText = `Напрям: ${trainer.specialization}`;
		modalWindow.querySelector('.modal__text').innerText = trainer.description;

		document.body.append(modalWindow);
		disableScroll();

		const modalElement = document.querySelector('.modal');

		modalElement.addEventListener('click', (event) => {
			if (event.target.closest('.modal__close') || event.target === modalElement) {
				modalElement.remove();
				enableScroll();
			}
		});

	}
});

// Sorting trainer cards
let id = 1;
const copyData = [...DATA];

sortingButtons.forEach(button => {
	button.setAttribute('data-id', `${id}`);
	id++;
})

function sortingData(array) {
	sortingSection.addEventListener('click', (event) => {
		const sortBtn = event.target.closest('.sorting__btn');
		const buttonId = sortBtn.dataset.id;

		if(!sortBtn.classList.contains('sorting__btn--active')){
			sortingButtons.forEach(button => {
				button.classList.remove('sorting__btn--active')
			})
		}

		if (event.target !== event.currentTarget) {
			event.target.classList.add("sorting__btn--active");
		}

		switch (buttonId) {
			case "2": {
				array.sort((a, b) => a["last name"].localeCompare(b["last name"]));
				break;
			}
			case "3": {
				array.sort((a, b) => parseInt(b.experience) - parseInt(a.experience));
				break;
			}
		}
		trainerCardsContainer.innerHTML = '';

		if (buttonId === '1'){
			renderFilteredCards(copyData);
		} else {
			renderCards(array);
		}
	});
}
sortingData(DATA);


//Filter trainer cards
function filterBySpecialization(specialization, array) {
	let filteredArray;

	switch (specialization) {
		case "all-direction": {
			filteredArray = array;
			break;
		}
		case "gym": {
			filteredArray = array.filter(item => item.specialization === 'Тренажерний зал');
			break;
		}
		case "fight-club": {
			filteredArray = array.filter(item => item.specialization === 'Бійцівський клуб');
			break;
		}
		case "kids-club": {
			filteredArray = array.filter(item => item.specialization === 'Дитячий клуб');
			break;
		}
		case "swimming-pool": {
			filteredArray = array.filter(item => item.specialization === 'Басейн');
			break;
		}
	}

	return filteredArray;
}

function filterByCategory(category, array) {
	let filteredArray;

	switch (category) {
		case "all-category": {
			filteredArray = array;
			break;
		}
		case "master": {
			filteredArray = array.filter(item => item.category === 'майстер');
			break;
		}
		case "specialist": {
			filteredArray = array.filter(item => item.category === 'спеціаліст');
			break;
		}
		case "instructor": {
			filteredArray = array.filter(item => item.category === 'інструктор');
			break;
		}
	}

	return filteredArray;
}

function renderFilteredCards(array) {
	const selectedSpecialization = document.querySelector('input[name="direction"]:checked').id;
	const selectedCategory = document.querySelector('input[name="category"]:checked').id;

	const filteredBySpecialization = filterBySpecialization(selectedSpecialization, array);
	const filteredCards = filterByCategory(selectedCategory, filteredBySpecialization);

	trainerCardsContainer.innerHTML = '';

	renderCards(filteredCards);

	return filteredCards;
}
sidebarForm.addEventListener('submit', (event) => {
	event.preventDefault();

	sortingData(renderFilteredCards(DATA));
});