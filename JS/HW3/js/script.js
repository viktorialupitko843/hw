"use strict";

/*
Попросіть користувача ввести свій вік за допомогою prompt.
Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.
Завдання з підвищенною складністю.
Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.
 */

const userAge = +prompt('Enter your age, please');

if (!userAge || isNaN(userAge)){
    alert('Error!');
} else if (userAge > 0 && userAge <= 120) {
    if (userAge < 12){
        alert('You are a child!');
    } else if (userAge < 18) {
        alert('You are a teenager!');
    } else {
        alert('You are an adult');
    }
} else {
    alert('Enter the correct data!');
}

/*
Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
скільки днів у цьому місяці. Результат виводиться в консоль.
 */

const month = prompt('Enter a month of the year').toLowerCase();

switch (month){
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        console.log('Цей місяць має 31 день');
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        console.log('Цей місяць має 30 день');
        break;
    case "лютий":
        console.log('Цей місяць має 28 день');
        break;
    default:{
        console.log('Enter the correct data!')
    }
}
