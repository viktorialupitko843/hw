## 1. Як можна оголосити змінну у Javascript?

Змінну у JavaScript можна оголосити трьома способами, за допомогою ключових слів var 
(застарілий спосіб, не рекомендується використовувати), let (використовується для змінних, 
які можна перезаписати), const (використовується для змінних які не можна перезаписати, тобто для констант).

## 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?

Рядок (string) - це рядок в програмуванні, тобто послідовність символів.
Його можна створити трьома способами:
1. За допомогою одинарних лапок: ' ';
2. За допомогою подвійних лапок: " ";
3. За допомогою вивернутих лапок: ``;

## 3. Як перевірити тип даних змінної в JavaScript?

Перевірити тип даних змінної в JavaScript можна перевірити за допомогою
методу typeof

## 4. Поясніть чому '1' + 1 = 11.

Тому що в цьому прикладі відбувається неявне перетворення числа в рядок (String),
потім два рядки склеюються, тобто відбувається конкатенація.

З іншими математичними операціями, таких як -,*,/,%,** такого не відбувається.
Там навпаки рядок перетворюється в числовий тип даних і виконується певна математична операція
