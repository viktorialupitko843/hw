"use strict";
// 1. Оголосіть змінну і присвойте в неї число.
// Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

const number = 11;
console.log(typeof number);

// 2. Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.
// Виведіть повідомлення у консоль у форматі `Мене звати {ім'я}, {прізвище}` використовуючи ці змінні.

const name = 'Victoria';
const lastName = 'Lupitko';

console.log(`Мене звати ${name} ${lastName}`);

// 3. Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.

const numOfCats = 3;
console.log(`There are ${numOfCats} cats on the tree`);