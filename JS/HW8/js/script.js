"use strict";

/*
1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift"
та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.
 */

const wordArray = ["travel", "hello", "eat", "ski", "lift"];

const filteredWords = wordArray.filter(word => word.length > 3);

console.log(filteredWords.length);

/*
2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}.
 Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
Відфільтрований масив виведіть в консоль.
 */

const peopleArray = [
    { name: "Іван", age: 25, sex: "чоловіча" },
    { name: "Анастасія", age: 20, sex: "жіноча" },
    { name: "Олександр", age: 40, sex: "чоловіча" },
    { name: "Анна", age: 38, sex: "жіноча" }
];
const filteredPeople = peopleArray.filter(person => person.sex === "чоловіча");

console.log(filteredPeople);

/*
- Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null],
і другим аргументом передати 'string', то функція поверне масив [23, null].
 */
function filterBy(arr, dataType) {
    return arr.filter(function(item) {
        return typeof item !== dataType;
    });
}

const inputArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(inputArray, 'string');

console.log(filteredArray);