/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/
const tabsBtn = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');

tabsBtn.forEach(onTabClick);

function onTabClick(item) {
    item.addEventListener('click',  () => {
        let currentBtn = item;
        let tabId = currentBtn.getAttribute('data-tab');
        let currentTab = document.querySelector(tabId);

        if (!currentBtn.classList.contains('active')){
            tabsBtn.forEach((item) => {
                item.classList.remove('active');
            });
            tabsItems.forEach((item) => {
                item.classList.remove('active');
            });
            currentBtn.classList.add('active');
            currentTab.classList.add('active');
        }

    });
}