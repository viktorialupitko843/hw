## 1. Що таке події в JavaScript і для чого вони використовуються?

Подія – це сигнал від браузера, що щось сталося. Всі DOM-вузли подають такі сигнали (хоча події бувають не тільки в
DOM).

Події використовуються для створення взаємодії із користувачем та для реагування на зміни в додатку або сторінці. Вони
дозволяють викликати функції або виконувати код у відповідь на конкретні дії користувача чи інші зміни у середовищі
виконання. Важливо використовувати обробники подій для створення динамічних та інтерактивних веб-додатків.

## 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

Події миші:

- click - коли миша клікає на елемент (на сенсорних пристроях це
  генерується при торканні).
`element.addEventListener('click', function() {
  console.log('Клікнуто!');
  });`
- Подвійний клік (dblclick) - виникає при подвійному кліці лівою кнопкою миші на елементі.
- contextmenu - коли користувач правою кнопкою миші клікає на
  елемент.
- mouseover / mouseout - коли курсор миші наводиться /
  залишається на елементі.
- mousedown / mouseup - коли кнопка миші натискана /
  відпускається над елементом.
- mousemove - коли миша рухається.

## 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Подія "contextmenu" виникає, коли користувач натискає праву кнопку миші для виведення контекстного меню. Ця подія
дозволяє вам визначати власні дії або взаємодії, які мають статися при відкритті контекстного меню на елементі сторінки
або додатку.