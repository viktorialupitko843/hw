/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

const btnClick = document.querySelector('#btn-click');
const section = document.querySelector('#content');

btnClick.addEventListener('click', () => {
    const p = document.createElement('p');
    p.innerText = 'New Paragraph';
    btnClick.before(p);
})


const newBtn = document.createElement('button');
newBtn.setAttribute('id', 'btn-input-create');
newBtn.innerText = 'Click to input'
section.append(newBtn);

newBtn.addEventListener('click', () => {
    const input = document.createElement('input');
    input.setAttribute('placeholder', 'Input your text');
    input.setAttribute('name', 'text');
    input.setAttribute('value', 'Enter your data');
    section.append(input);
})
