## 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Для створення вузлів DOM існують два методи:

- document.createElement(tag) - Створює новий вузол елемента з вказаним тегом
- document.createTextNode(text) - Створює новий вузол тексту із заданим текстом


- insertAdjacentHTML(position, text) - Цей метод дозволяє вставляти HTML-рядок в певному місці вказаного елементу.
  position може бути "beforebegin", "afterbegin", "beforeend" або "afterend". Наприклад:

## 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

Спочатку потрібно знайти елемент, який хочемо видалити. Можна використовувати метод document.querySelector або інші
методи вибору елементів: `var elementToRemove = document.querySelector('.navigation')`;
Коли ви знайшли елемент, ви можете видалити його, викликавши метод `remove()`
Перед видаленням елементу потрібно перевірити чи він існує, щоб уникнути помилок:
`if (elementToRemove) {
elementToRemove.remove();
}`

## 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

- node.append(...nodes або strings) – додає вузли або рядки в кінець вузла,
- node.prepend(...nodes або strings) – вставляє вузли або рядки в початок
вузла,
- node.before(...nodes або strings) – вставляє вузли або рядки перед
вузлом,
- node.after(...nodes або strings) – вставляє вузли або рядки після вузла,
- node.replaceWith(...nodes або strings) – замінює вузол заданими вузлами
або рядками.

Для додавання елементу ми можемо використовувати ще один досить
універсальний метод: elem.insertAdjacentHTML(where, html).
Перший параметр - це кодове слово, яке вказує, де вставити відносно
elem. Має бути одним із наступних:
- "beforebegin" – вставити html безпосередньо перед elem,
- "afterbegin" – вставити html в elem, на початку,
- "beforeend" – вставити html в elem, в кінці,
- "afterend" – вставити html безпосередньо після elem.
Другий параметр - це рядок HTML, який вставляється "як HTML".