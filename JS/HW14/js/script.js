
const toggleButton = document.querySelector('.toggle-theme-button');

toggleButton.addEventListener('click', toggleTheme);
function toggleTheme() {
    document.body.classList.toggle('dark-theme');

    // Зберігання інформації про тему в localStorage
    const isDarkTheme = document.body.classList.contains('dark-theme');
    localStorage.setItem('isDarkTheme', isDarkTheme);
}

const savedTheme = localStorage.getItem('isDarkTheme');
if (savedTheme === 'true') {
    document.body.classList.add('dark-theme');
}
