import { Route, Routes } from 'react-router-dom';
import HomePage from "./src/pages/HomePage.jsx";
import CartPage from "./src/pages/CartPage.jsx";
import FavouritePage from "./src/pages/FavouritePage.jsx";

const AppRoutes = ({products, addToFavorites, toggleOpenModal, favourites, cart, onDelete}) => {
  return(
      <>
          <Routes>
              <Route path='/' element={ <HomePage products={products}
                                                  toggleOpenModal={toggleOpenModal}
                                                  addToFavorites={addToFavorites}/> }/>
              <Route path='/cart' element={ <CartPage cart={cart} onDelete={onDelete} /> }/>
              <Route path='/favourite' element={ <FavouritePage favourites={favourites} /> }/>
          </Routes>
      </>
  )
}

export default AppRoutes;