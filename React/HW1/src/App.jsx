import {useEffect, useState} from 'react'
import Button from "./components/Button/index.jsx";
import ModalImage from "./components/Modal/ModalImage.jsx";
import ModelText from "./components/Modal/ModelText.jsx";
import Header from "./components/Header/index.jsx";
import ProductsContainer from "./components/ProductsContainer/index.jsx";
import ProductItem from "./components/ProductItem/index.jsx";
import './App.scss'
import AppRoutes from "../AppRoutes.jsx";

const LS_PRODUCTS_KEY = 'products';
const LS_CART_KEY = 'cart';


function App() {

    const [products, setProducts] = useState([]);
    const [openSecondModal, setOpenSecondModal] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [cart, setCart] = useState([]);


    useEffect(() => {
        const productsFromLS = localStorage.getItem(LS_PRODUCTS_KEY);

        if (productsFromLS) {
            setProducts(JSON.parse(productsFromLS));
        }else {
            const fetchProducts = async () => {

                try {
                    const data = await fetch('./products.json').then(res => res.json());
                    setProducts(data);
                } catch (e) {
                    console.log(e)
                }
            }
            fetchProducts();
        }

    }, [])

    useEffect(() => {
        const cartFromLS = localStorage.getItem(LS_CART_KEY);

        if (cartFromLS) {
            setCart(JSON.parse(cartFromLS))
        }

    }, []);

    const addToCart = async (id) => {
        setCart((prevState) => {
            const copyCart = structuredClone(prevState)
            const elem = copyCart.find(el => el.id === id);

            if (!elem) {
                copyCart.push({
                    ...products.find(el => el.id === id),
                    quantity: 1,
                });
            } else {
                elem.quantity += 1;
            }

            localStorage.setItem(LS_CART_KEY, JSON.stringify(copyCart))
            return copyCart;

        })
    }
    const addToFavorites = (id) => {
        setProducts((prevState) => {
            const clonedState = structuredClone(prevState)
            const el = clonedState.find(el => el.id === id);
            el.isFavourite = !el.isFavourite

            localStorage.setItem(LS_PRODUCTS_KEY, JSON.stringify(clonedState))
            return clonedState;
        });
    };

    const favoriteCount = products.filter(product => product.isFavourite).length;
    const cartCount = cart.reduce((total, item) => total + item.quantity, 0);

    const toggleOpenModal = (product) => {
        setSelectedProduct(product);
        setOpenSecondModal(true);
    }

    const favourites = products.filter(product => product.isFavourite);

    const deleteFromCart = async (id) => {
        setCart((prevState) => {
            const filteredCart = prevState.filter(item => item.id !== id);
            localStorage.setItem(LS_CART_KEY, JSON.stringify(filteredCart))
            return filteredCart;
        });
    };

    return (
    <>

        <Header cartCount={cartCount} favoriteCount={favoriteCount}/>

        <main>
            <AppRoutes products={products}
                       toggleOpenModal={toggleOpenModal}
                       addToFavorites={addToFavorites}
                       favourites={favourites}
                       cart={cart}
                       onDelete={deleteFromCart}
            />
        </main>


        {openSecondModal && selectedProduct && (
            <ModelText
                setOpenSecondModal={setOpenSecondModal}
                product={selectedProduct}
                addToCart={addToCart}
            />
        )}


    </>
  )
}

export default App
