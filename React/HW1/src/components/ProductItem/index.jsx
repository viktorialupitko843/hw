import styles from './ProductItem.module.scss'
import StarIcon from "../../svg/StarIcon/index.jsx";
import Button from "../Button/index.jsx";
import PropTypes from 'prop-types';
const ProductItem = ({item = {}, toggleOpenModal = () => {}, addToFavorites= () => {}}) => {
    return(
        <>
            <div className={styles.itemContainer}>
                <div className={`${styles.starIconContainer} ${item.isFavourite ? styles.favourite : ''}`}
                     onClick={() => { addToFavorites(item.id)}}>
                    <StarIcon/>
                </div>
                <img className={styles.image} src={item.img} alt="product"/>
                <h3>{item.name}</h3>
                <p>{item.price} UAN</p>
                <Button onClick={toggleOpenModal} >Add to cart</Button>
            </div>
        </>

    )
}

ProductItem.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        img: PropTypes.string.isRequired,
        isFavourite: PropTypes.bool,
    }).isRequired,
    toggleOpenModal: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
};

export default ProductItem;