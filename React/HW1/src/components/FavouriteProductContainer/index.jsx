import styles from './FavouriteProductContainer.module.scss'
import PropTypes from 'prop-types';
import ProductItem from "../ProductItem/index.jsx";
import FavouriteProductItem from "../FavouriteProductItem/index.jsx";
const FavouriteProductContainer = ({favourites}) => {
    return(
        <>
            <div className={styles.container}>
                {favourites.map(favourite => <FavouriteProductItem key={favourite.id} item={favourite}/>)}
            </div>
        </>

    )
}
FavouriteProductContainer.propTypes = {

};
export default FavouriteProductContainer;