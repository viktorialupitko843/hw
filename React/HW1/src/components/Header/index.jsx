import styles from './Header.module.scss'
import logo from './logo.jpg'
import SearchIcon from "../../svg/SearchIcon/index.jsx";
import CartIcon from "../../svg/CartIcon/index.jsx";
import HeartIcon from "../../svg/HeartIcon/index.jsx";
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom';

const Header = ({cartCount, favoriteCount}) => {
    return (
        <>
            <div className={styles.headerContainer}>
                <img src={logo} alt="logotype" style={{height: '150px'}}/>
                <ul className={styles.headerNavigation}>
                    <li>
                        <NavLink className={({isActive}) => isActive ? styles.active : ''} to="/">Store</NavLink>
                    </li>
                    <li>Apple</li>
                    <li>Samsung</li>
                    <li>Xiaomi</li>
                </ul>
                <div className={styles.search}>
                    <SearchIcon/>
                    <input
                        type="text"
                        placeholder="Search"
                    />
                </div>
                <div className={styles.cartContainer}>
                    <div className={styles.iconWithCount}>
                        <Link to='/cart'>
                            <CartIcon/>
                        </Link>
                        <span>{cartCount}</span>
                    </div>
                    <div className={styles.iconWithCount}>
                        <Link to='/favourite'>
                            <HeartIcon/>
                        </Link>
                        <span>{favoriteCount}</span>
                    </div>
                </div>

            </div>

        </>
    )
}
Header.propTypes = {
    cartCount: PropTypes.number.isRequired,
    favoriteCount: PropTypes.number.isRequired
};

export default Header;