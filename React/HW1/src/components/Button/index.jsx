import PropTypes from 'prop-types';

const Button = ({type="button",  classNames, onClick, children}) => {
  return (
      <>
          <button type={type} className={classNames} onClick={onClick}>
              {children}
          </button>
      </>
  )
}
Button.propTypes = {
    type: PropTypes.string,
    classNames: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node.isRequired,
};

export default Button;