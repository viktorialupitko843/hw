
import PropTypes from 'prop-types';
import ProductItem from "../ProductItem/index.jsx";
import FavouriteProductItem from "../FavouriteProductItem/index.jsx";
const CartContainer = ({cart, onDelete}) => {
    return(
        <>
            <div>
                {cart.map(item => <FavouriteProductItem key={item.id} item={item} onDelete={onDelete}/>)}
            </div>
        </>

    )
}
CartContainer.propTypes = {

};
export default CartContainer;