import Modal from "./Modal.jsx";
import ModalWrapper from "./ModalWrapper.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";

import PropTypes from 'prop-types';

const ModelText = ({setOpenSecondModal, addToCart, product}) => {
    const toggleCart = () => {
        addToCart(product.id);
        setOpenSecondModal(false);
    }

    return(
        <>

            <Modal onClick={() => setOpenSecondModal(false)}>
                <ModalWrapper>
                    <ModalHeader>
                        <h1> {product?.name} </h1>
                    </ModalHeader>
                    <ModalBody>
                        <p>Description for you product</p>
                    </ModalBody>
                    <ModalClose onClick={() => setOpenSecondModal(false)}></ModalClose>
                    <ModalFooter
                        firstClick={toggleCart}
                        firstText="ADD TO CART"></ModalFooter>
                </ModalWrapper>
            </Modal>
        </>
    )
}

ModelText.propTypes = {
    setOpenSecondModal: PropTypes.func.isRequired,
};
export default ModelText;