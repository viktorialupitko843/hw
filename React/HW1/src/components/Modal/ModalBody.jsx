import PropTypes from 'prop-types';

const ModalBody = ({ children }) => {
    return <div className="modalBody">{children}</div>;
};

ModalBody.propTypes = {
    children: PropTypes.node.isRequired,
};

export default ModalBody;
