import PropTypes from 'prop-types';

const ModalFooter = ({ firstText, secondText, firstClick, secondClick }) => {
    return (
        <div className="modalFooter">
            {firstText && <button className='firstBtn' onClick={firstClick}>{firstText}</button>}
            {secondText && <button className='secondBtn' onClick={secondClick}>{secondText}</button>}
        </div>
    );
};

ModalFooter.propTypes = {
    firstText: PropTypes.string.isRequired,
    secondText: PropTypes.string,
    firstClick: PropTypes.func.isRequired,
    secondClick: PropTypes.func,
};

export default ModalFooter;
