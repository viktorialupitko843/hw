import PropTypes from 'prop-types';

const ModalClose = ({onClick}) => {
    return (
        <button onClick={onClick} className="modalClose" >x</button>
    );
};

ModalClose.propTypes = {
    onClick: PropTypes.func.isRequired,
};

export default ModalClose;
