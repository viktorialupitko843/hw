import PropTypes from 'prop-types';

const ModalHeader = ({children}) => {
    return(
        <>
            <div className="modalHeader">{children}</div>
        </>
    )
}
ModalHeader.propTypes = {
    children: PropTypes.node
};

export default ModalHeader;