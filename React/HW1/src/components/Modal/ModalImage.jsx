import Modal from "./Modal.jsx";
import ModalWrapper from "./ModalWrapper.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";

import PropTypes from 'prop-types';

const ModalImage = ({setOpenFirstModal}) => {
    return(
        <>
            <Modal onClick={() => setOpenFirstModal(false)}>
                <ModalWrapper>
                    <ModalHeader>
                        <div className="img"></div>
                        <h1>Product Delete!</h1>
                    </ModalHeader>
                    <ModalBody>
                        <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
                    </ModalBody>
                    <ModalClose onClick={() => setOpenFirstModal(false)}></ModalClose>
                    <ModalFooter firstClick={() => setOpenFirstModal(false)} firstText="NO, CANCEL" secondText="YES, DELETE"></ModalFooter>
                </ModalWrapper>
            </Modal>

        </>
    )
}

ModalImage.propTypes = {
    setOpenFirstModal: PropTypes.func.isRequired,
};
export default ModalImage;