import PropTypes from 'prop-types';

const Modal = ({children , onClick}) => {
  return(
      <>
          <div className="modalWindow" >
              <div className="modalContent">
                  {children}
              </div>
          </div>
          <div className='background' onClick={onClick} ></div>
      </>
  )
}

Modal.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func.isRequired,
};

export default Modal;
