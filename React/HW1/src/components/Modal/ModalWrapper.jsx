import PropTypes from 'prop-types';

const ModalWrapper = ({children}) => {
  return(
      <>
          <div className="modalWrapper">{children}</div>
      </>
  )
}

ModalWrapper.propTypes = {
    children: PropTypes.node
};

export default ModalWrapper;