import styles from './ProductsContainer.module.scss'
import ProductItem from "../ProductItem/index.jsx";
import PropTypes from 'prop-types';
const ProductsContainer = ({products = [], toggleOpenModal = () => {}, addToFavorites = () => {}}) => {
  return(
      <>
        <div className={styles.container}>
            {products.map(product => <ProductItem
                addToFavorites={addToFavorites}
                key={product.id}
                item={product}
                toggleOpenModal={() => toggleOpenModal(product)}/>)}
        </div>
      </>

  )
}
ProductsContainer.propTypes = {
    products: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
            name: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            img: PropTypes.string,
            isFavourite: PropTypes.bool,
        })
    ).isRequired,
    toggleOpenModal: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
};
export default ProductsContainer;