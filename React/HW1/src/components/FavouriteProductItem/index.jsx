import styles from './FavouriteProductItem.module.scss'
import PropTypes from 'prop-types';

const FavouriteProductItem = ({item = {}, onDelete}) => {
    return(
        <>
            <div>
                <img className={styles.image} src={item.img} alt="product"/>
                <h3>{item.name}</h3>
                <p>{item.price} UAN</p>
                <span>{item?.quantity}</span>
                <button onClick={() => onDelete(item.id)}>
                    <svg width="20" height="20" viewBox="0 0 20 20">
                        <line x1="1" y1="19" x2="19" y2="1" stroke="black" strokeWidth="2"/>
                        <line x1="1" y1="1" x2="19" y2="19" stroke="black" strokeWidth="2"/>
                    </svg>
                </button>
            </div>
        </>

    )
}
FavouriteProductItem.propTypes = {

};
export default FavouriteProductItem;