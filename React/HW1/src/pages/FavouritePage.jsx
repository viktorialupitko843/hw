import FavouriteProductContainer from "../components/FavouriteProductContainer/index.jsx";

const FavouritePage = ({favourites}) => {
    return(
        <>
            <FavouriteProductContainer favourites={favourites}/>
        </>
    )
}
export default FavouritePage;