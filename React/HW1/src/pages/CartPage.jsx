import CartContainer from "../components/CartContainer/index.jsx";


const CartPage = ({cart, onDelete}) => {
    return(
        <>
            <div>
                <CartContainer cart={cart} onDelete={onDelete}/>
            </div>
        </>
    )
}
export default CartPage;