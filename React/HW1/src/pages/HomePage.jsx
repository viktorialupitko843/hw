import ProductsContainer from "../components/ProductsContainer/index.jsx";

const HomePage = ({products, addToFavorites, toggleOpenModal}) => {
  return(
      <>
          <ProductsContainer
              products={products}
              toggleOpenModal={toggleOpenModal}
              addToFavorites={addToFavorites}
          />
      </>
  )
}
export default HomePage;